package OOP;

import java.util.ArrayList;
import java.util.Scanner;

public class First {

	static ArrayList<Pro> computer = new ArrayList<>();

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		
		Pro A1 = new Pro("20,000", "HP", 1);
		Pro A2 = new Pro("50,000", "MAC BOOK", 2);
		Pro A3 = new Pro("40,000", "HUAWEI MATEBOOK", 3);
		Pro A4 = new Pro("30,000", "FUJITSU", 4);
		Pro A5 = new Pro("20,000", "DELL", 5);
		Pro A6 = new Pro("40,000", "LENOVO", 6);
		Pro A7 = new Pro("25,000", "ACER", 7);
		Pro A8 = new Pro("56,000", "ASUS", 8);
		Pro A9 = new Pro("54,000", "MSI", 9);
		Pro A10 = new Pro("32,000", "Mi Laptop", 10);
		

		
		computer.add(A1);
		computer.add(A2);
		computer.add(A3);
		computer.add(A4);
		computer.add(A5);
		computer.add(A6);
		computer.add(A7);
		computer.add(A8);
		computer.add(A9);
		computer.add(A10);
		

		System.out.println("List computer brand : " + computer);
		System.out.println("Please input your ID to delete(1-10) : ");
		int computerId = input.nextInt();

		for (int id = 0; id <= computer.size(); id++) {
			if (computerId == id) {
				computer.remove(id - 1);

				System.out.println("ID computer : " + computerId + " about to be deleted.");
				System.out.println("ID computer : " + computerId + " successfully deleted.");
				System.out.println("");
				System.out.println("List computer brand : " + computer);
			}
		}

		System.out.println("");

		System.out.println("Do you want to add ID ? (Y or N) ");
		String ans = input.next();
		System.out.println("");
		String ans2 = ans.toUpperCase(); 
		

		if (ans2.equals("Y")) {
			System.out.println("Please input ID to add : ");
			int computerId2 = input.nextInt(); 
			addID(computerId2);
		} else if (ans2.equals("N")) {
			System.out.println("Thank you.");
		}

	}

	public static void addID(int numberID) {
		for (int id = 0; id < numberID; id++) {
			computer.add(id + 1, null);
		}
		System.out.println("ID computer : " + numberID + " to add.");
		System.out.println("");
		System.out.println("List computer brand : " + computer);
		System.out.println("");
		System.out.println("Thank you.");
	}
	
}
